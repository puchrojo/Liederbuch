# Isaac Infos
https://tiiny.host/manage  - Da habe ich es hochgeladen
https://peach-carmine-45.tiiny.site/   - Das ist meine Seite

   git add -f Videos/**/**/*.vtt Videos/**/**/*.srt Videos/**/**/*.sbv

Erzeugen den Lyrics.txt ohne Zeiten, für Musik

   cp file.srt file.txt
   sed -i 's/\[.*]//' file.txt 

Erzeugen den Lyrics.txt auf Video (Youtube) Untertitel

https://ebby.co/subtitle-tools/converter/vtt-to-txt

Sync Videos mit 4pir2, danach soll über gitlab laufen

   cd ~/liederbuch/
   rsync -avz Videos/* root@4pir2:/var/www/Liederbuch/Videos/
   

# Template for Writing an eBook

- Has book template(asciidoc) for creating html, pdf, epub/mobi.
- Uses [asciidoctor](http://asciidoctor.org) to make the book.
- See `master.adoc`


## Edit


```bash
git clone git://github.com/rochacbruno/asciidoctor-book-template.git
cd asciidoctor-book-template
vim master.doc chapters/about.adoc chapters/preface.adoc
```

Once all your files are edited.


## Generate the book

```bash
docker run -it -v $PWD:/documents/ asciidoctor/docker-asciidoctor

# Inside docker container bash

# PDF
asciidoctor-pdf -vwt -o output/mybook.pdf master.adoc

# EPUB
asciidoctor-epub3 -vwt -o output/mybook.epub master.adoc

# HTML
asciidoctor -vwt -o output/mybook.html master.adoc
```

## Auto run on every file change

```bash
find . -name '*.adoc' | entr docker run --rm -v $PWD:/documents/ asciidoctor/docker-asciidoctor asciidoctor-pdf -vwt -o output/mybook.pdf master.adoc 
```

## Learn more

- [Quick guide on syntaxes](http://asciidoctor.org/docs/asciidoc-syntax-quick-reference/)


![Sample](./sample.png)
rsync -avz Videos/* gitlab-runner@4pir2.puchrojo.eu:/var/www/Liederbuch/Videos/

## 2024 Versucht

https://euantorano.co.uk/asciidoctor-documentation-gitlab/


Damit kann man das Bilden, aber es wird nur ein Artifact erzeugt, nicht in gitlab publiziert
https://gitlab.com/puchrojo/Liederbuch/-/jobs/8571407352
Aber die Seite bleibt leer:
https://puchrojo.gitlab.io/liederbuch/ 


