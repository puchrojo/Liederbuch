1
00:00:00,000 --> 00:00:09,103
Donde rompían las olas, y el cielo se arrepentía

2
00:00:26,342 --> 00:00:49,382
¿Sabes?
Sospecho que tengo la culpa de verme llorando.
Que parte del tiempo que invierto y que pierdo en quererte,
me está destrozando.

3
00:00:49,382 --> 00:01:09,706
Y me creo que el viento no sigue soplando cuando yo le cierro
Me creo que el mundo no sigue girando
si no estoy yo allí para verlo.

4
00:01:15,769 --> 00:01:36,782
Hay un trozo de luna que nunca me dice su nombre
Me escondo esperando que entonces pregunte por mí,
pero no responde.

5
00:01:36,782 --> 00:01:49,806
Se queda encendido si apago la luz.
Me ciega los ojos si apago la luz.

6
00:02:11,474 --> 00:02:35,086
Tienes la mala costumbre de hacer que no tenga sentido.
Que el tiempo que paso estudiándome tu recorrido
se pierda al amanecer.

7
00:02:35,086 --> 00:02:56,992
Y no distingo si te he visto.
Si existes o acaso es que somos lo mismo los dos.

8
00:03:40,906 --> 00:03:52,224
Y solo me ha dado un silencio que ya no lo aguanto.
Me pone en el pecho la noche lo llena de ti.

9
00:03:52,224 --> 00:04:15,000
Y juega a que crea que puedo volver a intentarlo,
y me pone en la boca su tacto mojado
y el gesto fruncido que nunca entendí.
Que no te entiendo.

