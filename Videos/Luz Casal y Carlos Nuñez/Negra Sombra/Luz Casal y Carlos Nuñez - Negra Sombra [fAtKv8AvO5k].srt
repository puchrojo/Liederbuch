1
00:00:45,500 --> 00:00:51,140
Cando penso que te fuches,

2
00:00:52,380 --> 00:00:58,480
negra sombra que me asombras,

3
00:00:59,520 --> 00:01:03,680
ó pé dos meus cabezales

4
00:01:04,060 --> 00:01:12,600
tornas facéndome mofa.

5
00:01:13,700 --> 00:01:18,320
Cando maxino que es ida,

6
00:01:20,140 --> 00:01:25,780
no mesmo sol te me amostras,

7
00:01:27,200 --> 00:01:31,080
i eres a estrela que brila,

8
00:01:31,080 --> 00:01:39,080
i eres o vento que zoa.

9
00:01:40,460 --> 00:01:46,100
Si cantan, es ti que cantas,

10
00:01:46,680 --> 00:01:52,340
si choran, es ti que choras,

11
00:01:52,340 --> 00:01:57,720
i es o marmurio do río

12
00:01:57,720 --> 00:02:04,680
i es a noite i es a aurora.

13
00:02:05,600 --> 00:02:11,440
En todo estás e ti es todo,

14
00:02:11,440 --> 00:02:17,740
pra min i en min mesma moras,

15
00:02:18,620 --> 00:02:23,620
nin me deixaras ti nunca,

16
00:02:23,620 --> 00:02:30,780
sombra que sempre me asombras.

17
00:02:31,440 --> 00:02:38,040
nin me deixaras ti nunca,

18
00:02:38,920 --> 00:02:46,840
sombra que sempre me asombras.

19
00:02:46,840 --> 00:02:48,840
-

20
00:03:36,180 --> 00:03:42,520
Si cantan, es ti que cantas,

21
00:03:42,520 --> 00:03:47,960
si choran, es ti que choras,

22
00:03:47,960 --> 00:03:53,000
i es o marmurio do río

23
00:03:53,000 --> 00:03:59,560
i es a noite i es a aurora.

24
00:04:01,140 --> 00:04:07,160
En todo estás e ti es todo,

25
00:04:07,300 --> 00:04:12,960
pra min i en min mesma moras,

26
00:04:13,440 --> 00:04:18,560
nin me deixaras ti nunca,

27
00:04:19,060 --> 00:04:25,580
sombra que sempre me asombras,

28
00:04:27,040 --> 00:04:34,360
nin me deixarás ti nunca...

29
00:04:35,700 --> 00:04:45,260
sombra que sempre me asombras...

