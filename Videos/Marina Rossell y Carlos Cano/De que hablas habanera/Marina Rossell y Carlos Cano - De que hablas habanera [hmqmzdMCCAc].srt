1
00:00:14,040 --> 00:00:18,580
De qué hablas habanera
cajita de la nostalgia

2
00:00:18,580 --> 00:00:26,180
canción de seda
los que un día te cantaron

3
00:00:26,529 --> 00:00:29,730
los que fueron y volvieron
que trajeron aquí

4
00:00:29,730 --> 00:00:34,700
que llevaron allá.

5
00:00:34,700 --> 00:00:45,420
Dónde esta el color, la calor,
el olor, el charol, el vaivén de La Habana.

6
00:00:45,640 --> 00:00:55,280
Habanera, la canción sencilla es buena
por lo que dice y lo que esconde;

7
00:00:55,290 --> 00:01:01,480
pero que esconde la habanera.

8
00:01:01,480 --> 00:01:11,460
Historias negras, de entreguerras
nació la canción de seda.

9
00:01:11,460 --> 00:01:19,240
De historias tristes
amarradas a su vera.

10
00:01:19,360 --> 00:01:27,240
Y un faro que en alta mar
iluminaba a la pena

11
00:01:27,560 --> 00:01:35,400
se cruzaba con los barcos
y acunaban la pobreza.

12
00:01:35,400 --> 00:01:46,140
Dónde esta el color, la calor,
el olor, el charol, el vaivén de La Habana.

13
00:01:46,340 --> 00:02:01,480
Habanera, por que no cuentas,
que naciste en tiempos de guerra.

14
00:02:19,900 --> 00:02:31,500
Dónde esta el color, la calor,
el olor, el charol, el vaivén de La Habana

15
00:02:31,500 --> 00:02:38,470
Habanera, de que no hablas

16
00:02:38,470 --> 00:02:52,670
de que no hablas
como te olvidaste de eso.

