1
00:01:02,389 --> 00:01:08,589
Podría pasarme la vida lamiéndome las heridas

2
00:01:09,026 --> 00:01:14,737
Y aún no cicatrizarían

3
00:01:15,680 --> 00:01:21,911
Mejor me levanto y salgo de este estéril letargo

4
00:01:22,508 --> 00:01:28,699
Y vuelvo a empezar a empezar a creer que hay alguna opción de ganar

5
00:01:28,815 --> 00:01:34,256
No me importa si eres listo o idiota

6
00:01:34,855 --> 00:01:41,516
Te voy a querer igual

7
00:01:41,959 --> 00:01:48,300
Si apareces ahora mismo entre los peces

8
00:01:48,417 --> 00:01:54,868
Te voy a perdonar cualquier pecado mortal

9
00:01:55,659 --> 00:02:02,369
Podría por fin demostrarse que todo es un sinsentido

10
00:02:02,370 --> 00:02:09,143
Y aún no existirían los caminos

11
00:02:09,144 --> 00:02:15,730
Podría hacerme leer la mano seis veces al año

12
00:02:15,731 --> 00:02:21,762
Y aún no sabría dónde ir

13
00:02:21,763 --> 00:02:28,102
No me importa si eres listo o idiota

14
00:02:28,103 --> 00:02:34,351
Te voy a querer igual

15
00:02:35,483 --> 00:02:41,829
Si apareces ahora mismo entre los peces

16
00:02:41,829 --> 00:02:48,084
Te voy a perdonar cualquier pecado mortal

17
00:02:49,064 --> 00:02:51,853
Veremos cine italiano

18
00:02:51,854 --> 00:02:55,174
O si prefieres cine francés

19
00:02:55,175 --> 00:03:01,956
Nos buscaremos las cosquillas
Las agotaremos en un mes

20
00:03:02,266 --> 00:03:08,756
Toda mi ambición es verte
Es enredar la noche de ayer

21
00:03:08,757 --> 00:03:14,064
Mi casa vacía es tu casa vacía y yo

22
00:03:17,216 --> 00:03:22,836
Te voy a querer

23
00:03:45,145 --> 00:03:50,695
No me importa si eres listo o idiota

24
00:03:51,378 --> 00:03:58,139
Te voy a querer igual

25
00:03:58,316 --> 00:04:05,147
Si apareces ahora mismo entre los peces

26
00:04:05,148 --> 00:04:11,317
Te voy a perdonar cualquier pecado mortal

27
00:04:11,789 --> 00:04:18,420
Me conformaré con ver la vida pasar

28
00:04:18,549 --> 00:04:25,910
Nada de esto será trascendental
